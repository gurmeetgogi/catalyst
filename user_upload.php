<?php
include('class.dbconnect.php');
$passingParam ="";
//Database host
$passingParam .= "h:";
//Database Name
$passingParam .= "d:";
//Database Username
$passingParam .= "u:";
//Database password
$passingParam .= "p:";

$mainParam = array("file:", "create_table", "dry_run","help");
$options = getopt($passingParam, $mainParam);
if (array_key_exists("h", $options) && array_key_exists("d", $options) && array_key_exists("u", $options)) {
    
    $DB = TRUE;
    $objdb= new dbconnect($options);
    // DB connection establish
    
}
if (array_key_exists("create_table", $options) && $DB) {
    $objdb->createUserTable();
    
}elseif (array_key_exists("dry_run", $options)) {
    $objdb->dryRun($options);
}elseif (array_key_exists("file", $options) && $DB) {
   $objdb->insertData($options);
 }elseif (array_key_exists("help", $options)) {
    help();
}else{
echo "catalyst script";
}

      /**
      * This Function for help the CLI params
      */  

       function help()
      {
          $data = [
              '--file'=>" [csv file name] - this is the name of the CSV to be parsed",
              '--create_table'=>'this will cause the MySQL users table to be built (and no further action will be taken)',
              '--dry_run'=>"this will be used with the --file directive in case we want to run the script but not 
              insert into the DB. All other functions will be executed, but the database won't be altered",
              '-h'=>'MySQL host',
              '-d'=>'MySql Database Name',
              '-u'=>'MySQL username',
              '-p'=>' MySQL Passowrd',
              '--help'=>'help command assign value true'
          ];

          foreach($data as $key=>$val){
              echo $key."\t".$val."\n\r";
          }
      }


?>