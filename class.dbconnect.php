<?php

class dbconnect {

    protected $dbhost;
    protected $dbusername;
    protected $dbpassword;
    protected $dbname;
    protected $dbconn;
    protected $resultset;
    protected $strsql;
    protected $csv;
    // constructor of the class with the passing argument from cli 
    public function __construct($options) {
        
        $this->dbname = $options['d'];
        $this->dbhost = $options['h'];
        $this->dbusername = $options['u'];
        $this->dbpassword = $options['p'];
        $this->connectdb();
        
    }


    // for opening the connection 
    public function connectdb() {
        try {
            $this->dbconn = mysqli_connect($this->dbhost, $this->dbusername, $this->dbpassword);
            mysqli_select_db($this->dbconn, $this->dbname);
          // echo "db connection establish\n\r"; 
        } catch (Exception $Ex) {
            print $this->objerrorhandler->showSysError(100, "System", $Ex->getMessage());
        }
    }

    // for closing the connection 
    public function closeconnection() {
        mysqli_close($this->dbconn);
    }

    // create  users

    public function createUserTable(){

        $strsql = "SHOW TABLES LIKE 'users'";
        $this->executequery($strsql);
        if($this->numrows() > 0)
        {
            echo "This table already exist";
        }
        
        else {
        $strsql="CREATE TABLE users (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(30) NOT NULL,
                surname VARCHAR(30) NOT NULL,
                email VARCHAR(50),
                UNIQUE KEY unique_email (email)
                )ENGINE=INNODB;";
        $this->executequery($strsql);
            echo "Table created\n\r";
        }
        

    }

    /**
     * This Function using for getting all Data from CSV file
     *
     * @param unknown_type $options     	: its Parameters from PHP CLI 
     * open the csv file and validate emailId and uppercase first character (Name and Surname)
     * set the email_status if correct email id for 1 otherwise 0
     */
    public function getCSVData($options)
    {
        if(file_exists($options['file']))
        {
        if (($handle = fopen($options['file'], "r")) !== FALSE) {
            $i=0;
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
               
                $status = 0;
                for ($c=0; $c < $num; $c++) {
                    if($c==0 || $c==1)
                    {
                       // user first char upper 
                       $csvdata =  ucfirst(strtolower($data[$c]));
                    }
                    else{
                        // email validate 
                        $isemail = trim($this->isemail($data[$c]));
                        $status = ($isemail) ? 1 : 0;
                        $csvdata =  $data[$c];
                    }
                    $this->csv[$i][$c] = $csvdata;
                }
                // set status email validate if correct set true othersiwe false.
                $this->csv[$i]['email_status'] = $status; 
                
                $i++;
            }
            fclose($handle);
        }
    
    }
    else{
        echo "File not found\n\r";
    }

    }


    /**
     * This Function using for display validate data from CSV file
     *
     * @param unknown_type $options     	: its Parameters from PHP CLI 
     * Print only correct format email data
     */
      
     public function dryRun($options)
     {
            // get all csv 
            $this->getCSVData($options); 
            foreach($this->csv as $rows)
            {
             if($rows['email_status']){
                 foreach($rows as $key=>$val)
                 {
                     if(!is_numeric($val))
                     {
                     echo $val."\t";
                     }
                 }
                 echo "\n\r";
                
             }
            }
            
     }       
       /**
      * This Function use to get all data from csv file and call saveData function for inserts Data into table
      *@param unknown_type $options     	: its Parameters from PHP CLI 
      */
     public function insertData($options)
     {
         $data=[];
            $this->getCSVData($options); 
            $stdout = fopen('php://stdout', 'w');
            foreach($this->csv as $rows)
            {
             if($rows['email_status']){
                 
                         $data['name'] = $rows[0];
                         $data['surname'] = $rows[1];
                         $data['email']= $rows[2];
                         $this->saveData("users",$data);
                     }
                     else{
                        fwrite(STDOUT,"$rows[2] is not correct format\n\r");
                    }
                
              }
              echo "Data inserted";
                
        } 

      
 
     /**
      * This Function inserts Data into table
      *
      * @param unknown_type $tableName	    : its Table name in which data to be inserted
      * @param unknown_type $arrData		: its assocsiative array like arr['Fieldname'] = value;
      * @return unknown						: returns 1 for sucess or errror message in case of fail
      */
    
            public function saveData($tableName,$arrData){
                
              $arrstringforsql['fields'] =  implode(",",array_keys($arrData));
              $dataField['value'] =  $this->codedata(array_values($arrData));
              $arrstringforsql['values'] ="";

              foreach($dataField['value'] as $val)
              {
                 if (trim(strtoupper($val)) == "NULL" or trim(strtoupper($val)) == "") {
                     $arrstringforsql['values'] .= "NULL,";
                 } else {
                     $arrstringforsql['values'] .= "'" . $val . "',";
                 }
              }
              
              $arrstringforsql['values'] = substr($arrstringforsql['values'], 0, -1);
              
              $strsql = "insert into $tableName (" . $arrstringforsql['fields'] . ") values (" . $arrstringforsql['values'] . ")";
              return $this->executequery($strsql);
              //print_r($arrstringforsql);
 
            }
 
           
            public function codedata($arrdata) {
                for ($i = 0; $i < count($arrdata); $i++)
                    $arrdata1[$i] = addslashes($arrdata[$i]);
                return $arrdata1;
            }
               

     /**
     * This Function using for validate emailID  
     *
     * @param  $emal     	: Passing emailId argument
     * check email format if correct return 1 otherwise 0
     */

     public function isemail($email) {

        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
        if (preg_match($regex, $email)) {
            return 1;
        }
        else{
            return 0;
        }

    //     if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //         return 1;
    //    }
    //    else{
    //        return 0;
    //    }
       
    }

    
    // execute query 
    public function executequery($strsql) {
        try {
            $this->strsql=$strsql;
            $this->resultset = mysqli_query($this -> dbconn,$this->strsql);
           
            return $this->resultset;
        } catch (Exception $Ex) {
            return "OOPS Error !!! <br>: " . $this->objerrorhandler->showSysError(100, "System", $Ex->getMessage() . "<br>" . $this->strsql);
        }
    }

    // find total number of records
    public function numrows() {
        return (mysqli_num_rows($this->resultset));
   }


    // print  query
    public function getQuery(){

        echo $this->strsql;
        
    }








}
?>